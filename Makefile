# TEX = pdflatex -shell-escape -interaction=nonstopmode -file-line-error
# PRE =  $(TEX) -ini -job-name="preamble" "&pdflatex preamble.tex\dump"
# BIB = bibtex
# pdflatex -shell-escape -interaction=nonstopmode ${filename}.tex
# bibtex proceedings
filename=CV


default:
	pdflatex ${filename}
	bibtex ths
	bibtex jrnl
	bibtex proc
	bibtex post
	pdflatex ${filename}
	pdflatex ${filename}

read:
	evince ${filename}.pdf &

clean:
	rm -f *.aux *.blg *.out *.bbl *.log *~ *.fdb_latexmk *.fls *.synctex.gz __latexindent_temp.tex
